package br.com.ronaldosr.usuario.gateway.decoder;

import br.com.ronaldosr.usuario.gateway.exception.CepNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ConsultaCepDecoder implements ErrorDecoder {

    private  ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            return new CepNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }
}
