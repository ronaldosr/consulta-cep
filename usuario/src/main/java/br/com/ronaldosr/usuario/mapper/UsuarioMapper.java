package br.com.ronaldosr.usuario.mapper;

import br.com.ronaldosr.usuario.dataprovider.model.Usuario;
import br.com.ronaldosr.usuario.controller.request.UsuarioRequest;
import br.com.ronaldosr.usuario.controller.response.UsuarioResponse;
import org.springframework.stereotype.Component;

@Component
public class UsuarioMapper {
    public Usuario converterParaUsuario(UsuarioRequest usuarioRequest) {
        Usuario usuario = new Usuario();
        usuario.setNome(usuarioRequest.getNome());
        return usuario;
    }

    public UsuarioResponse converterParaUsuarioResponse(Usuario usuario) {
        UsuarioResponse usuarioResponse = new UsuarioResponse();
        usuarioResponse.setId(usuario.getId());
        usuarioResponse.setNome(usuario.getNome());
        usuarioResponse.setLogradouro(usuario.getLogradouro());
        usuarioResponse.setBairro(usuario.getBairro());
        usuarioResponse.setCep(usuario.getCep());
        usuarioResponse.setLocalidade(usuario.getLocalidade());
        usuarioResponse.setUf(usuario.getUf());
        return usuarioResponse;
    }
}
