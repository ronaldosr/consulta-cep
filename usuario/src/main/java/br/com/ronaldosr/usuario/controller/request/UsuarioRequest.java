package br.com.ronaldosr.usuario.controller.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UsuarioRequest {

    @NotNull(message = "Informe o nome do usuário")
    @Size(min = 3, max = 50, message = "O nome do usuário deve conter entre 03 e 50 caracteres")
    private String nome;

    @NotNull(message = "Informe o CEP")
    @Size(min = 8, max = 8, message = "O CEP deve conter 8 dígitos, no formato '99999999'")
    private String cep;

    public UsuarioRequest() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }
}
