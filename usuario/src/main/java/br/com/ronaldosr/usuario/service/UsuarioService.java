package br.com.ronaldosr.usuario.service;

import br.com.ronaldosr.usuario.dataprovider.model.Usuario;
import br.com.ronaldosr.usuario.dataprovider.repository.UsuarioRepository;
import br.com.ronaldosr.usuario.gateway.ConsultaCepGateway;
import br.com.ronaldosr.usuario.gateway.data.CepData;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    ConsultaCepGateway consultaCepGateway;

    public Usuario cadastrarUsuario(Usuario usuario, String cep) {
        try {
            CepData cepData = consultaCepGateway.consultarCep(cep);
            usuario.setLogradouro(cepData.getLogradouro());
            usuario.setBairro(cepData.getBairro());
            usuario.setCep(cepData.getCep().replace("-",""));
            usuario.setLocalidade(cepData.getLocalidade());
            usuario.setUf(cepData.getUf());
            return usuarioRepository.save(usuario);
        } catch (FeignException e) {
            throw new RuntimeException("Erro no acesso ao CEP: " + e, e);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao cadastrar usuário: " + e, e);
        }
    }
}
