package br.com.ronaldosr.usuario.controller;

import br.com.ronaldosr.usuario.dataprovider.model.Usuario;
import br.com.ronaldosr.usuario.controller.request.UsuarioRequest;
import br.com.ronaldosr.usuario.controller.response.UsuarioResponse;
import br.com.ronaldosr.usuario.mapper.UsuarioMapper;
import br.com.ronaldosr.usuario.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private UsuarioMapper usuarioMapper;

    @PostMapping
    public UsuarioResponse cadastrarUsuario(@Valid @RequestBody UsuarioRequest usuarioRequest) {
        Usuario usuario = usuarioMapper.converterParaUsuario(usuarioRequest);
        return usuarioMapper.converterParaUsuarioResponse(
                usuarioService.cadastrarUsuario(usuario,usuarioRequest.getCep()));
    }
}
