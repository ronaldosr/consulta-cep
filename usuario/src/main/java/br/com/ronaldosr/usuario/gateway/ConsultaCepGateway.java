package br.com.ronaldosr.usuario.gateway;


import br.com.ronaldosr.usuario.gateway.config.ConsultaCepGatewayConfiguration;
import br.com.ronaldosr.usuario.gateway.data.CepData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CEP")
public interface ConsultaCepGateway {

    @GetMapping("/cep/{cep}")
    CepData consultarCep(@PathVariable String cep);
}
