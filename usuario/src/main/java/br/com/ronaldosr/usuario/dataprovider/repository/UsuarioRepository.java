package br.com.ronaldosr.usuario.dataprovider.repository;

import br.com.ronaldosr.usuario.dataprovider.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
}
