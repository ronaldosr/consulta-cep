package br.com.ronaldosr.usuario.gateway.config;

import br.com.ronaldosr.usuario.gateway.decoder.ConsultaCepDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ConsultaCepGatewayConfiguration {

    @Bean
    public ErrorDecoder consultarCep() {
        return new ConsultaCepDecoder();
    }
}
