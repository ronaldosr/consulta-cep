package br.com.ronaldosr.consultaCep.controller;

import br.com.ronaldosr.consultaCep.controller.response.CepResponse;
import br.com.ronaldosr.consultaCep.mapper.CepMapper;
import br.com.ronaldosr.consultaCep.service.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cep")
public class CepController {

    @Autowired
    private CepService cepService;

    @Autowired
    private CepMapper cepMapper;

    @GetMapping("/{cep}")
    public CepResponse consultarCep(@PathVariable String cep) {
        return cepMapper.converterParaCepResponse(cepService.consultarCep(cep));
    }

}
