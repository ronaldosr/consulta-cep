package br.com.ronaldosr.consultaCep.gateway;

import br.com.ronaldosr.consultaCep.gateway.data.CepData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "viaCep", url = "https://viacep.com.br/")
public interface ViaCepGateway {

    @GetMapping("/ws/{cep}/json/")
    CepData obterCep(@PathVariable String cep);
}
