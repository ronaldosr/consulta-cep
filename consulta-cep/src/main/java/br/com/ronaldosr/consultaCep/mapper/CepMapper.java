package br.com.ronaldosr.consultaCep.mapper;

import br.com.ronaldosr.consultaCep.controller.response.CepResponse;
import br.com.ronaldosr.consultaCep.gateway.data.CepData;
import org.springframework.stereotype.Component;

@Component
public class CepMapper {
    public CepResponse converterParaCepResponse(CepData cepData) {
        CepResponse cepResponse = new CepResponse();
        cepResponse.setCep(cepData.getCep());
        cepResponse.setLogradouro(cepData.getLogradouro());
        cepResponse.setBairro(cepData.getBairro());
        cepResponse.setLocalidade(cepData.getLocalidade());
        cepResponse.setUf(cepData.getUf());
        return cepResponse;
    }
}
