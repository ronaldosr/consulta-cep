package br.com.ronaldosr.consultaCep.service;

import br.com.ronaldosr.consultaCep.gateway.ViaCepGateway;
import br.com.ronaldosr.consultaCep.gateway.data.CepData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    private ViaCepGateway viaCepGateway;

    @NewSpan(name = "consultarCep")
    public CepData consultarCep(String cep) {
        return viaCepGateway.obterCep(cep);
    }
}
